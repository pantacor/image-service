FROM httpd:2.4.43-alpine

RUN apk update; apk add \
			jq \
			curl \
			bash \
			xz \
			sfdisk \
			pigz; \
	rm -rf /var/cache/apk/*

COPY files /
